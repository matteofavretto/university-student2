import static org.junit.Assert.assertEquals;

import org.junit.Test;

import university.University;

public class TestClass {
	
	University poli = new University("Politecnico di Torino");
	
	@Test
	public void testRector() {
		poli.setRector("Guido", "Saracco");
		assertEquals(poli.getRector(), "Guido Saracco");
	}
}